/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cudrado;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ToggleButton;
import javafx.scene.control.ToggleGroup;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class Cudrado extends Application {
    
    @Override
    public void start(Stage primaryStage) {
                       
        /* ----------Botones---------- */
        ToggleButton lineaBoton = new ToggleButton("Linea");
        ToggleButton rectanguloBoton = new ToggleButton("Rectangulo");
        
        ToggleButton[] barraHerramientas = {lineaBoton, rectanguloBoton};
        
        ToggleGroup herramientas = new ToggleGroup();
        
          for (ToggleButton herramienta : barraHerramientas) {
            herramienta.setMinWidth(90);
            herramienta.setToggleGroup(herramientas);
            herramienta.setCursor(Cursor.HAND);
        }
        
        ColorPicker colorRelleno = new ColorPicker(Color.TRANSPARENT);
        
        Button[] basicArr = {};

        for(Button btn : basicArr) {
            btn.setMinWidth(90);
            btn.setCursor(Cursor.HAND);
            btn.setTextFill(Color.WHITE);
            btn.setStyle("-fx-background-color: #666;");
        }
        
        VBox botones = new VBox(10);
        botones.getChildren().addAll(lineaBoton, rectanguloBoton);
        
        /* ----------Canvas---------- */
        Canvas canvas = new Canvas(800, 600);
        GraphicsContext gc;
        gc = canvas.getGraphicsContext2D();
        gc.setLineWidth(3);
        
        Line linea = new Line();
        Rectangle rectangulo = new Rectangle();
                        
        canvas.setOnMousePressed(e->{
            if(lineaBoton.isSelected()) {
                
                linea.setStartX(e.getX());
                linea.setStartY(e.getY());
            }
            else if(rectanguloBoton.isSelected()) { 
                gc.setFill(colorRelleno.getValue());
                rectangulo.setX(e.getX());                
                rectangulo.setY(e.getY());
            }
        });
        
        canvas.setOnMouseReleased(e->{
       
            if(lineaBoton.isSelected()) {
                linea.setEndX(e.getX());
                linea.setEndY(e.getY());
                gc.strokeLine(linea.getStartX(), linea.getStartY(), linea.getEndX(), linea.getEndY());
            }
            else if(rectanguloBoton.isSelected()) {
                rectangulo.setWidth(Math.abs((e.getX() - rectangulo.getX())));
                rectangulo.setHeight(Math.abs((e.getY() - rectangulo.getY())));
                
                if(rectangulo.getX() > e.getX()) {
                    rectangulo.setX(e.getX());
                }
               
                if(rectangulo.getY() > e.getY()) {
                    rectangulo.setY(e.getY());
                }

                gc.fillRect(rectangulo.getX(), rectangulo.getY(), rectangulo.getWidth(), rectangulo.getHeight());
                gc.strokeRect(rectangulo.getX(), rectangulo.getY(), rectangulo.getWidth(), rectangulo.getHeight());
            }  
        });
        
    
        /* ----------STAGE & SCENE---------- */
        BorderPane pane = new BorderPane();
        pane.setLeft(botones);
        pane.setCenter(canvas);
    
        Scene scene = new Scene(pane, 920, 600);
        
        primaryStage.setTitle("Cuadrado");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}